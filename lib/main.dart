import 'dart:js';

import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: 'Named Route Demo',
    initialRoute: '/',
    routes: {
      '/': (context) => const FristScreen(),
      '/secound': (context) => const SecoundScreen(),
      '/third': (context) => const ThirdScreen(),
    },
  ));
}

class FristScreen extends StatelessWidget {
  const FristScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Frist Screen'),
        ),
        body: Center(
            child: Column(
          children: [
            SizedBox(
              height: 10.0,
            ),
            ElevatedButton(
              child: const Text('Launch secound screen'),
              onPressed: () {
                Navigator.pushNamed(context, '/secound');
              },
            ),
            SizedBox(
              height: 10.0,
            ),
            ElevatedButton(
              child: const Text('Launch third screen'),
              onPressed: () {
                Navigator.pushNamed(context, '/third');
              },
            )
          ],
        )));
  }
}

class SecoundScreen extends StatelessWidget {
  const SecoundScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Secound Screen'),
        ),
        body: Center(
          child: ElevatedButton(
            child: const Text('Go back!'),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ));
  }
}

class ThirdScreen extends StatelessWidget {
  const ThirdScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Third Screen'),
        ),
        body: Center(
            child: Column(
          children: [
            SizedBox(
              height: 10.0,
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, '/secound');
              },
              child: const Text('Go Secound Screen!'),
            ),
            SizedBox(
              height: 10.0,
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: const Text('Go back!'),
            )
          ],
        )));
  }
}
